import { Component } from '@angular/core';

/**
 * Generated class for the About37Component component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'about37',
  templateUrl: 'about37.html'
})
export class About37Component {

  text: string;

  constructor() {
    console.log('Hello About37Component Component');
    this.text = 'Hello World';
  }

}
