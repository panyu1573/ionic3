import { NgModule } from '@angular/core';
import { About37Component } from './about37/about37';
@NgModule({
	declarations: [About37Component],
	imports: [],
	exports: [About37Component]
})
export class ComponentsModule {}
