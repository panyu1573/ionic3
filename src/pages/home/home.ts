import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// import {Order37Page} from '../order37/order37'
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  items : any = [
    {label:'action-sheets',link:'ActionSheetsPage'},
    {label:'alerts',link:'AlertsPage'},
    {label:'badges',link:'BadgesPage'},
    {label:'buttons',link:'ButtonsPage'},
    {label:'cards',link:'CardsPage'},
    {label:'checkboxes',link:'CheckboxesPage'},
    {label:'datetime',link:'DatetimePage'},
    {label:'fabs',link:'FabsPage'},
    {label:'gestures',link:'GesturesPage'},
    {label:'grid',link:'GridPage'},
    {label:'inputs',link:'InputsPage'},
    {label:'lists',link:'ListsPage'},
    {label:'loading',link:'LoadingPage'},
    {label:'menus',link:'MenusPage'},
    {label:'modals',link:'ModalsPage'},
    {label:'navigation',link:'NavigationPage'},
    {label:'popovers',link:'PopoversPage'},
    {label:'radios',link:'RadiosPage'},
    {label:'ranges',link:'RangesPage'},
    {label:'searchbars',link:'SearchbarsPage'},
    {label:'segments',link:'SegmentsPage'},
    {label:'selects',link:'SelectsPage'},
    {label:'slides',link:'SlidesPage'},
    {label:'tabs',link:'TabsPage'},
    {label:'toast',link:'ToastPage'},
    {label:'toggles',link:'TogglesPage'},
    {label:'toolbar',link:'ToolbarPage'},
  ];
  constructor(
    public navCtrl: NavController,
    // private Order37Page : Order37Page
    )
    {
      console.log(this.navCtrl.push)
      
    
  }
  ionViewDidLoad() {
    // setTimeout(()=>{
    //   this.navCtrl.push("NavigationPage");      
    // },500)
    console.log('ionViewDidLoad Order37Page');
  }
  itemSelected(item: object) {
    this.navCtrl.push(item.link);     
    console.log("Selected Item", item);
  }
}
